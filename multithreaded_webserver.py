#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
from evaluate import evaluate
import time


class ThreadingServer(ThreadingMixIn, HTTPServer):
    pass


class RequestHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        print "GET recieved"
        self.send_response(200)
        self.send_header('content-type', 'text/html;charset=utf-8')
        self.end_headers()
        answer = '<html><body><h1>Hello!</h1></body></html>'
        time.sleep(10)
        self.wfile.write(answer.encode('utf-8'))
        print "GET handled"

    def do_POST(self):
        print "POST recieved"
        self.send_response(200)
        self.send_header('content-type', 'application/xml;charset=utf-8')
        self.end_headers()
        length = int(self.headers['content-length'])
        data = self.rfile.read(length).decode("utf-8")
        sender = self.client_address[0] + ':' + str(self.client_address[1])
        print sender + ' - Recieved data: ' + data
        try:
            answer = evaluate(data)
            answer = '{"answer":%s}' % answer

        except Exception as err:
            print err
            answer = str(err)
        self.wfile.write(answer.encode('utf-8'))
        print 'send to client: ' + answer


def run(server_class = ThreadingServer, handler_class = RequestHandler):
        server_address = ('', 8000)
        httpd = server_class(server_address, handler_class)
        httpd.serve_forever()


if __name__ == '__main__':
    run()
