'''
Created on Jan 7, 2011

@author: keesey
'''
import unittest
from pymathema.nexml import NeXMLProcessor
from xml.dom import minidom
from xml.dom.minidom import parseString
from pymathema.evaluation import MATHML_NS_URI
from pymathema.namesonnodes import NAMES_ON_NODES_NS_URI


class Test(unittest.TestCase):


    def testTranslate(self):
        f = open("./Primates-PhySIC.xml", "r")
        nexml = minidom.parse(f)
        f.close()
        translator = NeXMLProcessor(nexml)
        assert translator.treeIDs == frozenset(["tree14616"])
        mathml = translator.translate()
        print mathml.documentElement.toprettyxml(encoding = "utf-8")

    def testCreateEvaluator(self):
        f = open("./Primates-PhySIC.xml", "r")
        nexml = minidom.parse(f)
        f.close()
        translator = NeXMLProcessor(nexml)
        evaluator = translator.createEvaluator()
        Hominoidea_def = parseString("<apply xmlns=\"" + MATHML_NS_URI + "\"><csymbol definitionURL=\"" + NAMES_ON_NODES_NS_URI + "#def-Clade\"/><apply><union/><csymbol definitionURL=\"http://www.ubio.org/authority/metadata.php?lsid=urn:lsid:ubio.org:namebank:2481711\"/><csymbol definitionURL=\"http://www.ubio.org/authority/metadata.php?lsid=urn:lsid:ubio.org:namebank:2481729\"/></apply></apply>")
        Hominoidea = evaluator.evaluate(Hominoidea_def.documentElement)
        hominoids = set()
        for value in evaluator.definitionURLTable.itervalues():
            try:
                if value <= Hominoidea:
                    hominoids.add(value)
            except:
                pass
        for hominoid in hominoids:
            print " : ".join(evaluator.findDefinitionURLs(hominoid))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testTranslator']
    unittest.main()