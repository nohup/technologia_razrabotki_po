'''
Created on Nov 12, 2010

@author: T. Michael Keesey
'''
import unittest
import re
from pymathema.taxonomy import createTaxonomy
from xml.dom import Node
from pymathema.namesonnodes import branch, clade, crown, synPrc,\
    NamesOnNodesEvaluator, maximal, minimal, subgraph, prcIntersect, prcUnion,\
    sucIntersect, sucUnion, total
from xml.dom.minidom import parse, parseString

def findIgnorableNodes(node):
    nodes = set()
    type = node.nodeType
    if type == Node.CDATA_SECTION_NODE or type == Node.TEXT_NODE:
        if re.match("^\s*$", node.nodeValue):
            nodes.add(node)
    elif type == Node.ELEMENT_NODE:
        for child in node.childNodes:
            nodes |= findIgnorableNodes(child)
    else:
        nodes.add(node)
    return nodes

def removeIgnorable(document):
    nodesToRemove = findIgnorableNodes(document.documentElement)
    for node in nodesToRemove:
        node.parentNode.removeChild(node)

def v(o):
    return frozenset([o])

def taxon(*vertexObjects):
    t = set()
    for o in vertexObjects:
        t |= v(o)
    return frozenset(t)

def assertTaxonEquality(actual, expected):
    if expected == actual:
        return
    if expected < actual:
        raise Exception("The expected taxon is a subtaxon of the actual result. The actual result also has: " + str(actual - expected))
    if actual < expected:
        raise Exception("The actual result is a subtaxon of the expected taxon. The expected taxon also has: " + str(expected - actual))
    if len(expected & actual) == 0:
        raise Exception("The sets are disjoint. Actual result: " + str(actual))
    raise Exception("The sets overlap but are not equal. Actual result also has: " + str(actual - expected) + ". Expected taxon also has: " + str(expected - actual))

class Test(unittest.TestCase):

    def evaluateXMLString(self, xmlString):
        return self.evaluator.evaluate(parseString(xmlString).documentElement)
    
    def performXMLTest(self, xmlString, expected):
        result = self.evaluateXMLString(xmlString)
        self.assertEqual(result, expected, "Expected " + str(expected) + ", but got " + str(result) + ".")
        return result
    
    def setUp(self):
        objects = ("Diadectes", "Dimorphodon", "Ichthyornis", "Pterodactylus", "Tyrannosaurus", "E. stenonis", "C. niloticus", "E. africanus", "E. ferus", "H. sapiens", "L. agilis", "O. anatinus", "S. camelus", "T. graeca", "T. major", "V. murinus", "V. gryphus", "mule", "hinny", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
        vertices = set()
        for o in objects:
            vertices.add(v(o))
        edges = set()
        edges.add((v(1), v("Diadectes")))
        edges.add((v(1), v(2)))
        edges.add((v(2), v(3)))
        edges.add((v(2), v(6)))
        edges.add((v(3), v("O. anatinus")))
        edges.add((v(3), v(4)))
        edges.add((v(4), v("H. sapiens")))
        edges.add((v(4), v(5)))
        edges.add((v(5), v("V. murinus")))
        edges.add((v(5), v("E. stenonis")))
        edges.add((v("E. stenonis"), v("E. africanus")))
        edges.add((v("E. stenonis"), v("E. ferus")))
        edges.add((v("E. africanus"), v("mule")))
        edges.add((v("E. ferus"), v("mule")))
        edges.add((v("E. africanus"), v("hinny")))
        edges.add((v("E. ferus"), v("hinny")))
        edges.add((v(6), v("T. graeca")))
        edges.add((v(6), v("L. agilis")))
        edges.add((v(6), v(7)))
        edges.add((v(7), v("C. niloticus")))
        edges.add((v(7), v(8)))
        edges.add((v(8), v(9)))
        edges.add((v(8), v(10)))
        edges.add((v(9), v("Dimorphodon")))
        edges.add((v(9), v("Pterodactylus")))
        edges.add((v(10), v("Tyrannosaurus")))
        edges.add((v(10), v(11)))
        edges.add((v(11), v("Ichthyornis")))
        edges.add((v(11), v(12)))
        edges.add((v(12), v("V. gryphus")))
        edges.add((v(12), v(13)))
        edges.add((v(13), v("S. camelus")))
        edges.add((v(13), v("T. major")))
        self.phylogeny = (frozenset(vertices), frozenset(edges))
        self.wings = taxon("V. murinus", "Dimorphodon", "Pterodactylus", "Ichthyornis", "V. gryphus", "T. major", 9, 11, 12, 13)
        self.extant = taxon("H. sapiens", "V. murinus", "E. africanus", "E. ferus", "mule", "hinny", "O. anatinus", "L. agilis", "T. graeca", "V. gryphus", "T. major", "S. camelus", "C. niloticus")

    def tearDown(self):
        self.phylogeny = None
        self.wings = None
        self.extant = None
        
    def testBranch(self):
        assertTaxonEquality(branch(self.phylogeny, v("V. gryphus"), v("Pterodactylus")), taxon("V. gryphus", 10, 11, 12))
        assertTaxonEquality(branch(self.phylogeny, taxon("V. gryphus", "H. sapiens"), v("Pterodactylus")), frozenset())
        
    def testClade(self):
        assertTaxonEquality(clade(self.phylogeny, v("E. ferus")), taxon("E. ferus", "mule", "hinny"))
        assertTaxonEquality(clade(self.phylogeny, taxon("H. sapiens", "O. anatinus")), taxon(3, 4, 5, "H. sapiens", "V. murinus", "O. anatinus", "E. stenonis", "E. africanus", "E. ferus", "mule", "hinny"))
        assertTaxonEquality(clade(self.phylogeny, taxon("mule", "hinny")), taxon("E. africanus", "E. ferus", "mule", "hinny"))
        assertTaxonEquality(clade(self.phylogeny, branch(self.phylogeny, v("V. gryphus"), v("Pterodactylus"))), taxon("V. gryphus", "T. major", "S. camelus", "Tyrannosaurus", "Ichthyornis", 10, 11, 12, 13))
        assertTaxonEquality(clade(self.phylogeny, synPrc(self.phylogeny, self.wings, v("V. gryphus"))), taxon("Ichthyornis", "V. gryphus", "T. major", "S. camelus", 11, 12, 13))
        
    def testCrown(self):
        Aves = taxon(12, 13, "V. gryphus", "T. major", "S. camelus")
        assertTaxonEquality(crown(self.phylogeny, taxon("V. gryphus", "Tyrannosaurus"), self.extant), Aves)
        assertTaxonEquality(crown(self.phylogeny, branch(self.phylogeny, v("V. gryphus"), v("Pterodactylus")), self.extant), Aves)
        assertTaxonEquality(crown(self.phylogeny, synPrc(self.phylogeny, self.wings, v("V. gryphus")), self.extant), Aves)
    
    def testFile(self):
        f = open("./namesonnodes.xml")
        mathml = parse(f)
        removeIgnorable(mathml)
        f.close()
        evaluator = NamesOnNodesEvaluator()
        math = evaluator.evaluate(mathml.documentElement)
        taxa = set()
        for value in math.itervalues():
            if isinstance(value, frozenset):
                taxa.add(value)
        taxonomy = createTaxonomy(taxa)
        print "----"
        print "digraph\n{"
        for edge in taxonomy[1]:
            print '\t"' + "::".join(evaluator.findDefinitionURLs(edge[0])) + "\" -> \""  + "::".join(evaluator.findDefinitionURLs(edge[1])) + '"'
        print "}"
        print "----"
                
    def testMaximal(self):
        assertTaxonEquality(maximal(self.phylogeny), taxon("Diadectes", "Dimorphodon", "Pterodactylus", "Tyrannosaurus", "Ichthyornis", "H. sapiens", "V. murinus", "mule", "hinny", "O. anatinus", "L. agilis", "T. graeca", "V. gryphus", "T. major", "S. camelus", "C. niloticus"))
        
    def testMinimal(self):
        assertTaxonEquality(minimal(self.phylogeny), v(1))
        s = subgraph(self.phylogeny, taxon("E. ferus", "E. africanus", "mule", "hinny", 12, "V. gryphus", 13, "T. major", "S. camelus"))
        assertTaxonEquality(minimal(s), taxon("E. ferus", "E. africanus", 12))
    
    def testNamesOnNodesEvaluator(self):
        Equus = taxon("E. stenonis", "E. africanus", "E. ferus", "mule", "hinny")
        Mammalia = taxon(3, 4, 5, "H. sapiens", "V. murinus", "O. anatinus", "E. stenonis", "E. africanus", "E. ferus", "mule", "hinny")
        self.evaluator = NamesOnNodesEvaluator(self.phylogeny)
        self.evaluator.addDefinition("#H. sapiens", taxon("H. sapiens"))
        self.evaluator.addDefinition("#O. anatinus", taxon("O. anatinus"))
        self.evaluator.addDefinition("#E. stenonis", taxon("E. stenonis"))
        self.evaluator.addDefinition("#E. africanus", taxon("E. africanus"))
        self.performXMLTest('<apply xmlns="http://www.w3.org/1998/Math/MathML"><csymbol definitionURL="http://namesonnodes.org/ns/math/2011#def-Clade"/><apply><union/><csymbol definitionURL="#H. sapiens"/><csymbol definitionURL="#O. anatinus"/></apply></apply>', Mammalia)
        self.evaluator.addDefinition("#MammaliaPhylogeny", subgraph(self.phylogeny, Mammalia)) 
        self.performXMLTest('<apply xmlns="http://www.w3.org/1998/Math/MathML"><apply><selector/><csymbol definitionURL="http://namesonnodes.org/ns/math/2011#def-Clade"/><csymbol definitionURL="#MammaliaPhylogeny"/></apply><apply><union/><csymbol definitionURL="#E. stenonis"/><csymbol definitionURL="#E. africanus"/></apply></apply>', Equus)

    def testPrcIntersect(self):
        assertTaxonEquality(prcIntersect(self.phylogeny, taxon("V. gryphus", "Pterodactylus")), taxon(1, 2, 6, 7, 8))
        
    def testPrcUnion(self):
        assertTaxonEquality(prcUnion(self.phylogeny, taxon("V. gryphus", "Pterodactylus")), taxon("V. gryphus", "Pterodactylus", 1, 2, 6, 7, 8, 9, 10, 11, 12))
        
    def testSucIntersect(self):
        assertTaxonEquality(sucIntersect(self.phylogeny, taxon("E. stenonis", "E. africanus")), taxon("E. africanus", "mule", "hinny"))
        
    def testSucUnion(self):
        assertTaxonEquality(sucUnion(self.phylogeny, taxon("E. stenonis", "E. africanus")), taxon("E. stenonis", "E. africanus", "E. ferus", "mule", "hinny"))
    
    def testSynPrc(self):
        assertTaxonEquality(synPrc(self.phylogeny, self.wings, v("V. gryphus")), taxon("V. gryphus", 11, 12))
        
    def testTotal(self):
        PanAves = taxon(8, 9, 10, 11, 12, 13, "Dimorphodon", "Pterodactylus", "Tyrannosaurus", "Ichthyornis", "V. gryphus", "T. major", "S. camelus")
        assertTaxonEquality(total(self.phylogeny, taxon("V. gryphus", "S. camelus"), self.extant), PanAves)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
