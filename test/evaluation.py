'''
Created on Oct 30, 2010

@author: T. Michael Keesey
'''
import unittest

from xml.dom.minidom import parseString
from pymathema.evaluation import MathMLEvaluator
from fractions import Fraction

import sys
sys.path.append('/home/stas/src/technologia_razrabotki_PO/pymathema')


def createFinestSet():
    return frozenset([object()])

class Test(unittest.TestCase):

    def testEvaluation(self):
        self.evaluator = MathMLEvaluator()
        self.evaluator.addDeclareType("set", createFinestSet)
        mathmlns = 'xmlns="http://www.w3.org/1998/Math/MathML"'
        self.performXMLTest("<cn " + mathmlns + ">10</cn>", 10)
        self.performXMLTest("<cn " + mathmlns + " type=\"real\">10</cn>", 10)
        self.performXMLTest("<cn " + mathmlns + " type=\"integer\">10</cn>", 10)
        self.performXMLTest("<cn " + mathmlns + " type=\"rational\">2<sep/>6</cn>", Fraction(1, 3))
        self.performXMLTest("<cn " + mathmlns + " type=\"complex-cartesian\">3<sep/>2</cn>", complex(3, 2))
        self.performXMLTest("<declare " + mathmlns + "><csymbol definitionURL=\"urn:foo:bar\"/><cn>12</cn></declare>", 12)
        self.performXMLTest("<csymbol " + mathmlns + " definitionURL=\"urn:foo:bar\"/>", 12)
        self.performXMLTest("<declare " + mathmlns + "><ci><msub><mi>A</mi><mn>1</mn></msub></ci><cn>54</cn></declare>", 54)
        self.performXMLTest("<ci " + mathmlns + "><msub><mi>A</mi><mn>1</mn></msub></ci>", 54)
        self.performXMLTest("<false " + mathmlns + "/>", False)
        self.performXMLTest("<true " + mathmlns + "/>", True)
        self.performXMLTest("<emptyset " + mathmlns + "/>", set())
        self.performXMLTest("<set " + mathmlns + "/>", set())
        self.performXMLTest("<set " + mathmlns + "><cn>1</cn><cn>1</cn><cn>2</cn></set>", frozenset([1, 2]))
        self.performXMLTest("<apply " + mathmlns + "><not/><false/></apply>", True)
        self.performXMLTest("<apply " + mathmlns + "><and/><true/><true/></apply>", True)
        self.performXMLTest("<apply " + mathmlns + "><and/><true/><false/></apply>", False)
        self.performXMLTest("<apply " + mathmlns + "><and/><false/><false/></apply>", False)
        self.performXMLTest("<apply " + mathmlns + "><or/><true/><true/></apply>", True)
        self.performXMLTest("<apply " + mathmlns + "><or/><true/><false/></apply>", True)
        self.performXMLTest("<apply " + mathmlns + "><or/><false/><false/></apply>", False)
        self.performXMLTest("<apply " + mathmlns + "><apply><compose/><not/><lt/></apply><cn>1</cn><cn>2</cn></apply>", False)
        a = self.evaluateXMLString("<declare " + mathmlns + " type=\"set\"><csymbol definitionURL=\"urn:foo:a\"/></declare>")
        self.evaluateXMLString("<declare " + mathmlns + " type=\"set\"><csymbol definitionURL=\"urn:foo:b\"/></declare>")
        self.evaluateXMLString("<declare " + mathmlns + " type=\"set\"><csymbol definitionURL=\"urn:foo:c\"/></declare>")
        self.performXMLTest("<csymbol " + mathmlns + " definitionURL=\"urn:foo:a\"/>", a)
        self.evaluateXMLString("<apply " + mathmlns + "><union/><csymbol definitionURL=\"urn:foo:a\"/><csymbol definitionURL=\"urn:foo:b\"/></apply>")
        self.evaluateXMLString("<apply " + mathmlns + "><union/><csymbol definitionURL=\"urn:foo:a\"/><csymbol definitionURL=\"urn:foo:c\"/></apply>")
        self.evaluateXMLString("<apply " + mathmlns + "><union/><csymbol definitionURL=\"urn:foo:b\"/><csymbol definitionURL=\"urn:foo:c\"/></apply>")
        self.evaluateXMLString("<apply " + mathmlns + "><union/><csymbol definitionURL=\"urn:foo:a\"/><csymbol definitionURL=\"urn:foo:b\"/><csymbol definitionURL=\"urn:foo:c\"/></apply>")
        self.performXMLTest("<apply " + mathmlns + "><setdiff/><csymbol definitionURL=\"urn:foo:a\"/><emptyset/></apply>", a)
        self.performXMLTest("<apply " + mathmlns + "><intersect/><csymbol definitionURL=\"urn:foo:a\"/><csymbol definitionURL=\"urn:foo:b\"/></apply>", frozenset())
        self.evaluator.defaultDomainOfApplication = set(range(0, 3))
        self.assertEqual(self.evaluator.universalPowerSet(), frozenset([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]))
        self.performXMLTest("<set " + mathmlns + "><bvar><ci>x</ci></bvar><condition><apply><lt/><ci>x</ci><cn>2</cn></apply></condition><ci>x</ci></set>", frozenset([0, 1]))
        self.performXMLTest("<apply " + mathmlns + "><set><list><cn>0</cn><cn>1</cn></list><list><cn>0</cn><cn>2</cn></list></set><cn>2</cn><cn>3</cn></apply>", False)
        self.performXMLTest("<apply " + mathmlns + "><set><list><cn>0</cn><cn>1</cn></list><list><cn>0</cn><cn>2</cn></list></set><cn>0</cn><cn>2</cn></apply>", True)
        
    def evaluateXMLString(self, xmlString):
        return self.evaluator.evaluate(parseString(xmlString).documentElement)
    
    def performXMLTest(self, xmlString, expected):
        result = self.evaluateXMLString(xmlString)
        self.assertEqual(result, expected, "Expected " + str(expected) + ", but got " + str(result) + ".")
        return result

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
