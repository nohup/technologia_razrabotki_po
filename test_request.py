#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import requests


headers = {'Content-Type': 'application/xml'} # set what your server accepts
mathmlns = 'xmlns="http://www.w3.org/1998/Math/MathML"'
expression = "<cn " + mathmlns + " type=\"rational\">2<sep/>6</cn>"
expression = "<cn " + mathmlns + " type=\"complex-cartesian\">3<sep/>2</cn>"
expression = "<declare " + mathmlns + "><csymbol definitionURL=\"urn:foo:bar\"/><cn>12</cn></declare>"
expression = "<apply " + mathmlns + "><not/><false/></apply>"
expression = "<apply " + mathmlns + "><factorial/><ci>5</ci></apply>"


print requests.post('http://127.0.0.1:8000', data=expression, headers=headers).text
