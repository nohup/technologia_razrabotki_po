#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from xml.dom.minidom import parseString
from pymathema.evaluation import MathMLEvaluator


def createFinestSet():
    return frozenset([object()])


evaluator = MathMLEvaluator()
evaluator.addDeclareType("set", createFinestSet)


def evaluate(expr):
    global evaluator
    return evaluator.evaluate(parseString(expr).documentElement)
