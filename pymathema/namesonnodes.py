'''
Created on Nov 10, 2010

@author: T. Michael Keesey
'''

from copy import copy
from pymathema.evaluation import MathMLEvaluator

_resultsCache = {}

_unitID = 0

NAMES_ON_NODES_NS_URI = "http://namesonnodes.org/ns/math/2011"

def clearResultsCache():
    '''For speed, results of most phylogenetic operations are cached.
    
    Calling this function clears that cache.
    '''
    _resultsCache = {}

class PhylogeneticOperation:
    '''Represents a phylogenetic operation.
    
    Phylogenetic operations may be called on their own, in which case the phylogenetic context is the phylogenetic graph that was passed during instantiation.
    
    Phylogenetic operations may also be invoked by passing another phylogenetic graph using bracket notation. Example:
    
    >>> ancestor = frozenset([0])
    >>> descendantA = frozenset([1])
    >>> descendantB = frozenset([2])
    >>> mainPhylogeny = (frozenset([ancestor, descendantA, descendantB]), frozenset([(ancestor, descendantA), (ancestor, descendantB)]))
    >>> subPhylogeny = (frozenset([ancestor, descendantA]), frozenset([(ancestor, descendantA)]))
    >>> opSucUnion = PhylogeneticOperation(mainPhylogeny, sucUnion)
    >>> opSucUnion(ancestor)
    frozenset([0, 1, 2])
    >>> opSucUnion[subPhylogeny](ancestor)
    frozenset([0, 1])
    
    The last two expressions are respectively equivalent to:
    >>> sucUnion(mainPhylogeny, ancestor)
    frozenset([0, 1, 2])
    >>> sucUnion(subPhylogeny, ancestor)
    frozenset([0, 1])
    '''
    def __init__(self, phylogeneticGraph, baseFunction):
        self._phylogeneticGraph = phylogeneticGraph
        self._baseFunction = baseFunction
    def __call__(self, args):
        a = [self._phylogeneticGraph] + list(args)
        return self._baseFunction(*a)
    def __getitem__(self, obj, cls=None):
        if obj == self._phylogeneticGraph:
            return self
        if len(obj) == 2:
            return PhylogeneticOperation(obj, self._baseFunction)
        raise Exception("Invalid selector argument for operation: " + str(obj))

class NamesOnNodesEvaluator(MathMLEvaluator):
    '''A MathML evaluator with predefined Names on Nodes entities for evaluating phylogenetic definitions.
    
    A phylogenetic graph may be passed in during instantiation or it may be specified in the MathML that is being evaluated. Example:
    
    <declare xmlns="http://www.w3.org/1998/Math/MathML">
        <csymbol definitionURL="http://namesonnodes.org/ns/math/2011#def-Phylogeny"/>
        <list>
            <set>
                <!-- vertices -->
            </set>
            <set>
                <!-- edges -->
            </set>
        </list>
    </declare>
    '''
    def __init__(self, phylogeneticGraph = None):
        MathMLEvaluator.__init__(self)
        declareUnit = lambda: frozenset([object()])
        self.addDeclareType("set", declareUnit)
        self.defaultDeclareType = declareUnit
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Union"] = lambda a: union(a[0])
        self.setPhylogeny(phylogeneticGraph)
    def addDefinition(self, definitionURL, value):
        if definitionURL == NAMES_ON_NODES_NS_URI + "#def-Phylogeny":
            self.setPhylogeny(value)
        MathMLEvaluator.addDefinition(self, definitionURL, value)
    def setPhylogeny(self, graph):
        if graph is None:
            graph = (frozenset([]), frozenset([]))
        self.defaultDomainOfApplication = graph[0]
        self._phylogeny = graph
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Branch"] = PhylogeneticOperation(graph, branch)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Clade"] = PhylogeneticOperation(graph, clade)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Crown"] = PhylogeneticOperation(graph, crown)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Finest"] = PhylogeneticOperation(graph, finest)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-ImPrc"] = PhylogeneticOperation(graph, imPrc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-ImSuc"] = PhylogeneticOperation(graph, imSuc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Maximal"] = PhylogeneticOperation(graph, maximal)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Minimal"] = PhylogeneticOperation(graph, minimal)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Phylogeny"] = graph
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Prc"] = PhylogeneticOperation(graph, prc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Prcs"] = PhylogeneticOperation(graph, prcs)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-PrcIntersect"] = PhylogeneticOperation(graph, prcIntersect)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-PrcUnion"] = PhylogeneticOperation(graph, prcUnion)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-PrPrc"] = PhylogeneticOperation(graph, prPrc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-PrSuc"] = PhylogeneticOperation(graph, prSuc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Subgraph"] = PhylogeneticOperation(graph, subgraph)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Suc"] = PhylogeneticOperation(graph, suc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Sucs"] = PhylogeneticOperation(graph, sucs)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-SucIntersect"] = PhylogeneticOperation(graph, sucIntersect)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-SucUnion"] = PhylogeneticOperation(graph, sucUnion)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-SynPrc"] = PhylogeneticOperation(graph, synPrc)
        self.definitionURLTable[NAMES_ON_NODES_NS_URI + "#def-Total"] = PhylogeneticOperation(graph, total)

def adjMatrix(graph, vertexList):
    '''Calculates an adjacency matrix.
    '''
    key = (adjMatrix, graph, tuple(vertexList))
    try:
        return _resultsCache[key]
    except Exception:
        pass
    basicRow = []
    while len(basicRow) < len(graph[0]):
        basicRow.append(False)
    indices = {}
    matrix = []
    for index, vertex in enumerate(vertexList):
        matrix.append(copy(basicRow))
        indices[vertex] = index
    for edge in graph[1]:
        headIndex = indices[edge[0]]
        tailIndex = indices[edge[1]]
        matrix[headIndex][tailIndex] = True
    matrix2 = []
    for row in matrix:
        matrix2.append(tuple(row))
    result = tuple(matrix2)
    _resultsCache[key] = result
    return result

def branch(graph, internalTaxon, externalTaxon):
    '''Calculates the branch (exclusive predecessors) of an internal taxon with respect to an external taxon.
    '''
    key = (branch, graph, internalTaxon, externalTaxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    internal = prcIntersect(graph, internalTaxon)
    if internal == frozenset():
        return internal
    result = internal - prcUnion(graph, externalTaxon)
    _resultsCache[key] = result
    return result

def clade(graph, taxon):
    '''Calculates the clade specified by a taxon.
    
    If the minimal units of the taxon form a cladogen, then the cladogen is united with all successors.
    Otherwise, this returns a node-based clade using the taxon's units as its specifiers.
    '''
    key = (clade, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    if sucIntersect(graph, minimal(subgraph(graph, taxon))) == frozenset():
        return sucUnion(graph, maximal(subgraph(graph, prcIntersect(graph, taxon))))
    result = sucUnion(graph, taxon)
    _resultsCache[key] = result
    return result

def crown(graph, boundingTaxon, extantTaxon):
    '''Calculates the crown clade as determined by a bounding taxon and the taxon of all extant units.
    '''
    return clade(graph, clade(graph, boundingTaxon) & extantTaxon)

def finest(graph, taxon):
    '''Converts a taxon into a set of its units. (Taxa are unions of units.)
    '''
    vertices = graph[0]
    key = (finest, taxon, vertices)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    result = set()
    for vertex in vertices:
        if vertex <= taxon:
            result.add(vertex)
    result = frozenset(result)
    _resultsCache[key] = result
    return result

def imPrc(graph, a, b):
    '''Determines whether unit A is an immediate predecessor (parent) of unit B.
    '''
    return (a, b) in graph[1]

def imSuc(graph, a, b):
    '''Determines whether unit A is an immediate successor (child) of unit B.
    '''
    return (b, a) in graph[1]

def maximal(graph):
    '''Returns the union of the maximal (that is, terminal or sink) units of the indicated graph. 
    '''
    key = (maximal, graph)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    vertices = set(graph[0].copy())
    for edge in graph[1]:
        vertices.discard(edge[0])
    result = union(vertices)
    _resultsCache[key] = result
    return result

def minimal(graph):
    '''Returns the union of the minimal (that is, initial or source) units of the indicated graph. 
    '''
    key = (minimal, graph)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    vertices = set(graph[0].copy())
    for edge in graph[1]:
        vertices.discard(edge[1])
    result = union(vertices)
    _resultsCache[key] = result
    return result

def prc(graph, a, b):
    '''Determines whether unit A is a predecessor of unit B.
    
    Holds if a == b.
    '''
    if a == b:
        return True;
    return (a, b) in transClosure(graph)[1]

def prcs(graph, vertex):
    '''Returns a set whose members are all predecessors of the specified vertex (including the vertex itself).
    '''
    key = (prcs, graph, vertex)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    closure = transClosure(graph)
    result = set()
    result |= vertex
    for u in graph[0]:
        if u != vertex and (u, vertex) in closure[1]:
            result |= u
    result = frozenset(result)
    _resultsCache[key] = result
    return result

def prcIntersect(graph, taxon):
    '''Determines a taxon's predecessor intersection.
    '''
    key = (prcIntersect, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    result = None
    for vertex in graph[0]:
        if vertex <= taxon:
            if result == None:
                result = set() | prcs(graph, vertex)
            else:
                result &= prcs(graph, vertex)
                if len(result) == 0:
                    return frozenset()
    if result == None:
        result = frozenset()
    else:
        result = frozenset(result)
    _resultsCache[key] = result
    return result

def prcUnion(graph, taxon):
    '''Determines a taxon's predecessor union.
    '''
    key = (prcUnion, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    result = set()
    for vertex in graph[0]:
        if vertex <= taxon:
            result |= prcs(graph, vertex)
    result = frozenset(result)
    _resultsCache[key] = result
    return result

def prPrc(graph, a, b):
    '''Determines whether unit A is a proper predecessor of unit B.
    
    Does not hold if a == b.
    '''
    return (a, b) in transClosure(graph)[1]

def prSuc(graph, a, b):
    '''Determines whether unit A is a proper successor of unit B.
    
    Does not hold if a == b.
    '''
    return (b, a) in transClosure(graph)[1]

def subgraph(graph, taxon):
    '''Finds the subgraph of a graph as determined by a taxon.
    
    Vertices of the original graph are only copied into the subgraph if they are subsets of the specified taxon.
    '''
    key = (subgraph, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    edges = set()
    vertices = set()
    try:
        for edge in graph[1]:
            if edge[0] <= taxon and edge[1] <= taxon:
                vertices.add(edge[0])
                vertices.add(edge[1])
                edges.add(edge)
    except:
        print "EDGE: " + str(edge)
        print "TAXON: " + str(taxon)
        raise
    result = (frozenset(vertices), frozenset(edges))
    _resultsCache[key] = result
    return result

def suc(graph, a, b):
    '''Determines whether unit A is a successor of unit B.
    
    Holds if a == b.
    '''
    if a == b:
        return True;
    return (b, a) in transClosure(graph)[1]

def sucs(graph, vertex):
    '''Returns a set whose members are all successors of the specified vertex (including the vertex itself).
    '''
    key = (sucs, graph, vertex)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    closure = transClosure(graph)
    result = set()
    result |= vertex
    for u in graph[0]:
        if u != vertex and (vertex, u) in closure[1]:
            result |= u
    result = frozenset(result)
    _resultsCache[key] = result
    return result

def sucIntersect(graph, taxon):
    '''Determines a taxon's successor intersection.
    '''
    key = (sucIntersect, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    result = None
    for vertex in graph[0]:
        if vertex <= taxon:
            if result == None:
                result = set() | sucs(graph, vertex)
            else:
                result &= sucs(graph, vertex)
                if len(result) == 0:
                    return frozenset()
    if result == None:
        result = frozenset()
    else:
        result = frozenset(result)
    _resultsCache[key] = result
    return result

def sucUnion(graph, taxon):
    '''Determines a taxon's successor union.
    '''
    key = (sucUnion, graph, taxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    result = set()
    for vertex in graph[0]:
        if vertex <= taxon:
            result |= sucs(graph, vertex)
    result = frozenset(result)
    _resultsCache[key] = result
    return result

def synPrc(graph, apomorphicTaxon, representativeTaxon):
    '''Determines the synapomorphic predecessors of a representative taxon, as determined by an apomorphic taxon.
    '''
    key = (synPrc, graph, apomorphicTaxon, representativeTaxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    if not (representativeTaxon <= apomorphicTaxon):
        return frozenset()
    subg = subgraph(graph, apomorphicTaxon)
    result = prcIntersect(subg, representativeTaxon)
    _resultsCache[key] = result
    return result

def total(graph, boundingTaxon, extantTaxon):
    '''Calculates the total clade as determined by a bounding taxon and the taxon of all extant units.
    '''
    key = (total, graph, boundingTaxon, extantTaxon)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    c = crown(graph, boundingTaxon, extantTaxon)
    result = clade(graph, branch(graph, c, extantTaxon - c))
    _resultsCache[key] = result
    return result

def transClosure(graph):
    '''Calculates the transitive closure of a directed, acyclic graph.
    '''
    key = (transClosure, graph)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    vertexList = list(graph[0])
    vertexRange = range(len(vertexList))
    am = adjMatrix(graph, vertexList)
    closureMatrix = []
    for row in am:
        closureMatrix.append(list(row))
    for k in vertexRange:
        for i in vertexRange:
            if closureMatrix[i][k]:
                for j in vertexRange:
                    closureMatrix[i][j] = closureMatrix[i][j] or (closureMatrix[i][k] and closureMatrix[k][j])
    edges = set()
    for i in vertexRange:
        for j in vertexRange:
            if closureMatrix[i][j]:
                edges.add((vertexList[i], vertexList[j]))
    result = (graph[0], frozenset(edges))
    _resultsCache[key] = result
    return result

def transReduction(graph):
    '''Calculates the transitive reduction of a directed, acyclic graph.
    '''
    key = (transReduction, graph)
    try:
        return _resultsCache[key]
    except Exception:
        pass
    closure = transClosure(graph)
    edges = set(closure[1])
    for x in closure[0]:
        for y in closure[0]:
            if x != y:
                for z in closure[0]:
                    if (x, z) in edges and (x, y) in edges and (y, z) in closure[1]:
                        edges.remove((x, z))
    result = (graph[0], frozenset(edges))
    _resultsCache[key] = result
    return result

def union(vertices):
    '''Returns the union of all members of a vertex set.
    '''
    result = set()
    for vertex in vertices:
        result |= vertex
    return frozenset(result)