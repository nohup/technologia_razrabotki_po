'''
Created on Jan 19, 2011

@author: T. Michael Keesey
'''
from xml.dom import Node

def cleanXML(node):
    for child in list(node.childNodes):
        type = child.nodeType
        if type == Node.ELEMENT_NODE:
            cleanXML(child)
        elif type != Node.TEXT_NODE and type != Node.CDATA_SECTION_NODE:
            node.removeChild(child)
        elif child.nodeValue is None or child.nodeValue.strip() == '':
            node.removeChild(child)

def getURLs(definitions, taxon):
    urls = set()
    for url, value in definitions.iteritems():
        if value == taxon:
            urls.add(url)
    urls = list(urls)
    urls.sort()
    return urls
