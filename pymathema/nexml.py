'''
Created on Jan 6, 2011

@author: keesey
'''
from xml.dom import XMLNS_NAMESPACE
from string import split
from pymathema.evaluation import MATHML_NS_URI, createMathMLDoc
import re
from pymathema.namesonnodes import NamesOnNodesEvaluator

DEFAULT_EQUIVALENCE_NAMES = set([
    ("http://www.w3.org/2004/02/skos/core#", "broadMatch"),
    ("http://www.w3.org/2004/02/skos/core#", "closeMatch"),
    ("http://www.w3.org/2004/02/skos/core#", "exactMatch"),
    ("http://www.w3.org/2004/02/skos/core#", "narrowMatch"),
    ("http://www.w3.org/2002/07/owl#", "sameAs"),
    (MATHML_NS_URI, "approx"),
    (MATHML_NS_URI, "eq"),
    (MATHML_NS_URI, "equivalent"),
])

STRICT_EQUIVALENCE_NAMES = set([
    ("http://www.w3.org/2004/02/skos/core#", "exactMatch"),
    ("http://www.w3.org/2002/07/owl#", "sameAs"),
    (MATHML_NS_URI, "eq"),
    (MATHML_NS_URI, "equivalent"),
])

NEXML_NS_URI = "http://www.nexml.org/1.0"

XSI_NS_URI = "http://www.w3.org/1999/XMLSchema-instance"

_ID_RE = re.compile("^\D+\d+$")

class NeXMLProcessor:
    def __init__(self, nexmlDoc, equivalenceNames = None):
        self._doc = nexmlDoc
        self._equivalenceNames = DEFAULT_EQUIVALENCE_NAMES if equivalenceNames is None else equivalenceNames
        self._readNamespaces()
        self._readTreeIDs()
    def createEvaluator(self, treeIDs = None):
        evaluator = NamesOnNodesEvaluator()
        for otu in self._doc.getElementsByTagNameNS(NEXML_NS_URI, "otu"):
            id = otu.getAttribute("id")
            unit = frozenset([object()])
            evaluator.addDefinition("#" + id, unit)
            self._equateMetaUnits(otu, unit, evaluator)
        nodes = set()
        edges = set()
        for tree in self._doc.getElementsByTagNameNS(NEXML_NS_URI, "tree"):
            if treeIDs is None or tree.getAttribute("id") in treeIDs:
                for node in tree.getElementsByTagNameNS(NEXML_NS_URI, "node"):
                    id = node.getAttribute("id")
                    if node.hasAttribute("otu"):
                        unit = evaluator.evaluateDefinitionURL("#" + node.getAttribute("otu"))
                    else:
                        unit = frozenset([object()])
                    evaluator.addDefinition("#" + id, unit)
                    self._equateMetaUnits(node, unit, evaluator)
                for edge in tree.getElementsByTagNameNS(NEXML_NS_URI, "edge"):
                    source = evaluator.evaluateDefinitionURL("#" + edge.getAttribute("source"))
                    target = evaluator.evaluateDefinitionURL("#" + edge.getAttribute("target"))
                    nodes.add(source)
                    nodes.add(target)
                    edges.add((source, target))
        evaluator.setPhylogeny((frozenset(nodes), frozenset(edges)))
        return evaluator
    def translate(self, treeIDs = None):
        mathml = createMathMLDoc()
        for otu in self._doc.getElementsByTagNameNS(NEXML_NS_URI, "otu"):
            id = otu.getAttribute("id")
            label = None
            if otu.hasAttribute("label"):
                label = otu.getAttribute("label") 
            self._createUnitDeclaration(mathml, id, label)
            self._equateMeta(mathml, otu, id)
        edges = set()
        for tree in self._doc.getElementsByTagNameNS(NEXML_NS_URI, "tree"):
            id = tree.getAttribute("id")
            if treeIDs is None or id in treeIDs:
                self._translateTree(mathml, tree, edges)
        self._writePhylogeny(mathml, edges)
        return mathml
    def _isResourceMeta(self, node):
        if node.namespaceURI == NEXML_NS_URI and node.localName == "meta":
            if node.hasAttributeNS(XSI_NS_URI, "type"):
                xsiType = self._resolveQName(node.getAttributeNS(XSI_NS_URI, "type"))
                return xsiType == (NEXML_NS_URI, "ResourceMeta")
        return False
    def _createIdentifierFromID(self, mathml, id):
        mo = mathml.createElementNS(MATHML_NS_URI, "mo")
        if _ID_RE.match(id):
            o = ""
            n = ""
            for c in id:
                if re.match("\d", c):
                    n += c
                else:
                    o += c
            msub = mathml.createElementNS(MATHML_NS_URI, "msub")
            mn = mathml.createElementNS(MATHML_NS_URI, "mn")
            mo.appendChild(mathml.createTextNode(o))
            mn.appendChild(mathml.createTextNode(n))
            msub.appendChild(mo)
            msub.appendChild(mn)
            mphantom = mathml.createElementNS(MATHML_NS_URI, "mphantom")
            mphantom.appendChild(msub)
            return mphantom
        mo.appendChild(mathml.createTextNode(id))
        return mo
    def _createIdentifierFromLabel(self, mathml, label):
        mi = mathml.createElementNS(MATHML_NS_URI, "mi")
        mi.appendChild(mathml.createTextNode(label))
        return mi
    def _createUnitDeclaration(self, mathml, id, label = None):
        declare = mathml.createElementNS(MATHML_NS_URI, "declare")
        declare.setAttributeNS(MATHML_NS_URI, "id", id)
        declare.setAttributeNS(MATHML_NS_URI, "type", "set")
        csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
        csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "#" + id)
        if label is None:
            csymbol.appendChild(self._createIdentifierFromID(mathml, id))
        else:
            csymbol.appendChild(self._createIdentifierFromLabel(mathml, label))
        declare.appendChild(csymbol)
        mathml.documentElement.appendChild(declare)
    def _createEquivalent(self, mathml, id, uri):
        declare = mathml.createElementNS(MATHML_NS_URI, "declare")
        csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
        csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", uri)
        declare.appendChild(csymbol)
        csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
        csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "#" + id)
        declare.appendChild(csymbol)
        mathml.documentElement.appendChild(declare)
    def _equateMeta(self, mathml, element, id):
        for meta in element.childNodes:
            if self._isResourceMeta(meta):
                rel = meta.getAttribute("rel")
                qRel = self._resolveQName(rel)
                if qRel in self._equivalenceNames:
                    href = meta.getAttribute("href")
                    self._createEquivalent(mathml, id, href)
    def _equateMetaUnits(self, element, unit, evaluator):
        for meta in element.childNodes:
            if self._isResourceMeta(meta):
                rel = meta.getAttribute("rel")
                qRel = self._resolveQName(rel)
                if qRel in self._equivalenceNames:
                    href = meta.getAttribute("href")
                    evaluator.addDefinition(href, unit)
    def _readNamespaces(self):
        element = self._doc.documentElement
        self._namespaceDict = {}
        for i in range(element.attributes.length):
            attr = element.attributes.item(i)
            if attr.namespaceURI == XMLNS_NAMESPACE:
                self._namespaceDict[attr.localName] = attr.value
            elif attr.name == "xmlns":
                self._defaultNamespaceURI = attr.value
    def _readTreeIDs(self):
        self.treeIDs = set()
        for tree in self._doc.getElementsByTagNameNS(NEXML_NS_URI, "tree"):
            self.treeIDs.add(tree.getAttribute("id"))
        self.treeIDs = frozenset(self.treeIDs)
    def _resolveQName(self, name):
        parts = split(name, sep = ":", maxsplit = 1)
        l = len(parts)
        if l == 0:
            return None
        if l == 1:
            return (self._defaultNamespaceURI, parts[0])
        uri = None
        if self._namespaceDict.has_key(parts[0]):
            uri = self._namespaceDict[parts[0]]
        else:
            uri = parts[0]
        return (uri, parts[1])
    def _translateTree(self, mathml, tree, edges):
        for node in tree.getElementsByTagNameNS(NEXML_NS_URI, "node"):
            id = node.getAttribute("id")
            if node.hasAttribute("otu"):
                otu = node.getAttribute("otu")
                self._createEquivalent(mathml, otu, "#" + id)
                self._equateMeta(mathml, node, otu)
            else:
                label = None
                if node.hasAttribute("label"):
                    label = node.getAttribute("label") 
                self._createUnitDeclaration(mathml, id, label)
                self._equateMeta(mathml, node, id)
        for edge in tree.getElementsByTagNameNS(NEXML_NS_URI, "edge"):
            source = edge.getAttribute("source")
            target = edge.getAttribute("target")
            edges.add((source, target))
    def _writePhylogeny(self, mathml, edges):
        edgeSet = mathml.createElementNS(MATHML_NS_URI, "set")
        nodeIDs = set()
        for edge in edges:
            nodeIDs.add(edge[0])
            nodeIDs.add(edge[1])
            edgeList = mathml.createElementNS(MATHML_NS_URI, "list")
            csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
            csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "#" + edge[0])
            edgeList.appendChild(csymbol)
            csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
            csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "#" + edge[1])
            edgeList.appendChild(csymbol)
            edgeSet.appendChild(edgeList)
        nodeSet = mathml.createElementNS(MATHML_NS_URI, "set")
        for nodeID in nodeIDs:
            csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
            csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "#" + nodeID)
            nodeSet.appendChild(csymbol)
        listNode = mathml.createElementNS(MATHML_NS_URI, "list")
        listNode.appendChild(nodeSet)
        listNode.appendChild(edgeSet)
        mo = mathml.createElementNS(MATHML_NS_URI, "mo")
        mo.appendChild(mathml.createTextNode("P"))
        csymbol = mathml.createElementNS(MATHML_NS_URI, "csymbol")
        csymbol.setAttributeNS(MATHML_NS_URI, "definitionURL", "http://namesonnodes.org/ns/math/2011#def-Phylogeny")
        csymbol.appendChild(mo)
        declare = mathml.createElementNS(MATHML_NS_URI, "declare")
        declare.appendChild(csymbol)
        declare.appendChild(listNode)
        mathml.documentElement.appendChild(declare)
    