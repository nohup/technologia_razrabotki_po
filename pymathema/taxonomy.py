'''
Created on Jan 7, 2011

@author: keesey
'''
from pymathema.namesonnodes import transReduction

def createTaxonomy(taxa):
    nodes = set()
    edges = set()
    for taxonA in taxa:
        nodes.add(taxonA)
        for taxonB in taxa:
            if taxonA != taxonB:
                if taxonA > taxonB:
                    edges.add((taxonA, taxonB))
                elif taxonB > taxonA:
                    edges.add((taxonB, taxonA))
    return transReduction((frozenset(nodes), frozenset(edges)))
