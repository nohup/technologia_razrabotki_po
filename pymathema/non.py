#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Created on Jan 19, 2011

@author: T. Michael Keesey
'''
import sys
from string import upper
from urllib import unquote_plus
sys.path.append("../")
from pymathema.nexml import NeXMLProcessor, NEXML_NS_URI
from pymathema.utils import cleanXML, getURLs
from xml.dom import minidom
from pymathema.namesonnodes import transReduction, NAMES_ON_NODES_NS_URI,\
    NamesOnNodesEvaluator, finest, sucUnion
import traceback
from pymathema.evaluation import MATHML_NS_URI

from optparse import OptionParser, OptionValueError

def exportDOT(taxonomy, definitionURLTable):
    sys.stdout.write("digraph Taxonomy {\n")
    for inclusion in taxonomy[1]:
        sys.stdout.write("\t\"" + " ".join(getURLs(definitionURLTable, inclusion[0])) + "\" -> \"" + " ".join(getURLs(definitionURLTable, inclusion[1])) + "\";\n")
    sys.stdout.write("}\n")

def exportHTML(taxonomy, definitionURLTable):
    sourceNodes = sources(taxonomy)
    sourceNames = []
    for source in sourceNodes:
        urls = getURLs(definitionURLTable, source)
        for url in urls:
            if url.startswith("#"):
                sourceNames.append(unquote_plus(url[1:]))
    if len(sourceNames) == 0:
        sourceNames.append("Unnamed Taxon")
    else:
        sourceNames.sort()
    sys.stdout.writelines([
        '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n',
        '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd">\n',
        '\t<head>\n',
        '\t\t<title>Taxonomy of ' + "/".join(sourceNames) + '</title>\n',
        '\t\t<meta http-equiv="Content-Language" content="en" />\n',
        '\t\t<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n',
        '\t\t<meta name="Generator" content="Names on Nodes" />\n',
        '\t\t<style type="text/css">\n',
        '\t\t\tli {list-style-type: circle}\n',
        '\t\t\tul {margin-bottom: 2px}\n',
        '\t\t\t.nomen {font-style: italic; margin: 1px; margin-right: 1em}\n',
        '\t\t\t.ref {text-decoration: none}\n',
        '\t\t</style>\n',
        '\t</head>\n',
        '\t<body>\n',
        '\t\t<ul class="taxonomy">\n'
    ])
    for source in sourceNodes:
        exportHTMLNode(source, taxonomy, definitionURLTable, "\t\t\t")
    sys.stdout.writelines([
        '\t\t</ul>\n'
        '\t</body>\n',
        '</html>'
    ])

def exportHTMLNode(unit, taxonomy, definitionURLTable, indent):
    #:TODO: Use internal links in case of multiple parents. 
    urls = getURLs(definitionURLTable, unit)
    sys.stdout.write(indent + '<li class="taxon">\n')
    sys.stdout.write(indent + '\t<span class="url-set">\n')
    for url in urls:
        if url.startswith("#"):
            sys.stdout.write(indent + '\t\t<a class="nomen" id="' + url[1:] + '">' + unquote_plus(url[1:]) + "</a>\n")
        else:
            sys.stdout.write(indent + '\t\t<a class="ref" href="' + url + '">&rarr;</a>\n')
    sys.stdout.write(indent + '\t</span>\n')
    successors = children(taxonomy, unit)
    if successors != frozenset():
        successors = list(successors)
        successors.sort(cmp = TaxonComparator(taxonomy, definitionURLTable))
        sys.stdout.write(indent + '\t<ul>\n')
        for child in successors:
            exportHTMLNode(child, taxonomy, definitionURLTable, indent + "\t\t")
        sys.stdout.write(indent + '\t</ul>\n')
    sys.stdout.write(indent + '</li>\n')

def children(graph, node):
    children = set()
    for arc in graph[1]:
        if arc[0] == node:
            children.add(arc[1])
    return children

def sources(graph):
    sources = set(graph[0])
    for arc in graph[1]:
        sources.remove(arc[1])
    return frozenset(sources)

class TaxonComparator():
    def __init__(self, taxonomy, definitions):
        self.taxonomy = taxonomy
        self.definitions = definitions
    def __call__(self, a, b):
        if a == b:
            return 0
        an = len(finest(self.taxonomy, sucUnion(self.taxonomy, a)))
        bn = len(finest(self.taxonomy, sucUnion(self.taxonomy, b)))
        if an == bn:
            astr = unicode(getURLs(self.definitions, a))
            bstr = unicode(getURLs(self.definitions, b))
            if astr == bstr:
                return 0
            if astr < bstr:
                return -1
            return 1
        return an - bn

def main():
    try:
        usage = "usage: %prog [options] PHYLOREFERENCE_FILE"
        parser = OptionParser(usage)
        parser.add_option("-c", "--context", metavar="FILE", help="use a MathML or NeXML file to define the phylogenetic context")
        parser.add_option("-d", "--definitions", metavar="FILE", default="", help="use a MathML file to define taxa based on units")
        parser.add_option("-r", "--reference", metavar="FILE", default="", help="use a MathML file to specify phyloreference(s)")
        parser.add_option("-f", "--format", metavar="FORMAT", default="DOT", help="Specify a file format: DOT (default), HTML")
        parser.add_option("-t", "--taxonomy", action="store_true", help="instead of searching for a phyloreference, export the taxonomic hierarchy")
        # :TODO: Add option for equivalence names.
        # :TODO: Add option for selecting tree IDs.
        (options, args) = parser.parse_args()
        if len(args) != 0:
            parser.error("Extraneous arguments provided.")
        if options.taxonomy and options.reference != "":
            raise OptionValueError("You cannot ask for the taxonomy and a phyloreference at the same time.")
        try:
            f = open(options.context, "r")
        except:
            raise OptionValueError("Could not find context file: " + str(options.context))
        try:
            xml = minidom.parse(f)
            cleanXML(xml)
        except:
            raise OptionValueError("Context file is not a valid XML file.")
        evaluator = None
        if xml.documentElement.namespaceURI == NEXML_NS_URI and xml.documentElement.localName == "nexml":
            processor = NeXMLProcessor(xml)
            evaluator = processor.createEvaluator()
        elif xml.documentElement.namespaceURI == MATHML_NS_URI:
            evaluator = NamesOnNodesEvaluator()
            evaluator.evaluate(xml.documentElement)
        else:
            raise OptionValueError("Unrecognized format for context file. Must be a MathML or NeXML file.")
        definedURLs = frozenset(evaluator.definitionURLTable.keys())
        if options.definitions != "":
            try:
                f = open(options.definitions, "r")
            except:
                raise OptionValueError("Could not find definitions file.")
            try:
                xml = minidom.parse(f)
                cleanXML(xml)
            except:
                raise OptionValueError("Definitions file is not a valid XML file.")
            if xml.documentElement.namespaceURI != MATHML_NS_URI:
                raise OptionValueError("Definitions file must be a MathML file.")
            evaluator.evaluate(xml.documentElement)
            definedURLs = frozenset(evaluator.definitionURLTable.keys()) - definedURLs
        entity = None
        if options.reference != "":
            try:
                f = open(options.reference, "r")
            except:
                raise OptionValueError("Could not find reference file.")
            try:
                xml = minidom.parse(f)
                cleanXML(xml)
            except:
                raise OptionValueError("Reference file is not a valid XML file.")
            if xml.documentElement.namespaceURI != MATHML_NS_URI:
                raise OptionValueError("Reference file must be a MathML file.")
            entity = evaluator.evaluate(xml.documentElement)
        taxa = set()
        empty = frozenset()
        for url, value in evaluator.definitionURLTable.iteritems():
            if value != empty and url in definedURLs and not url.startswith(NAMES_ON_NODES_NS_URI):
                taxa.add(value)
        inclusions = set()
        for a in taxa:
            for b in taxa:
                if a > b:
                    inclusions.add((a, b))
        taxonomy = transReduction((frozenset(taxa), frozenset(inclusions)))
        if options.taxonomy:
            format = upper(options.format)
            if format == "DOT":
                exportDOT(taxonomy, evaluator.definitionURLTable)
            elif format == "HTML":
                exportHTML(taxonomy, evaluator.definitionURLTable)
            else:
                raise Exception("Unrecognized file format: \"" + options.format + "\".")
        else:
            # :TODO: other formats
            sys.stdout.write(unicode(entity))
    except Exception:
        traceback.print_exc(file = sys.stderr)
        exit(-1)
    exit()
if __name__ == "__main__":
    main()